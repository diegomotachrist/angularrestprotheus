import { Cliente } from './../clientes-lista/cliente';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take, tap, delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private readonly API = 'http://localhost:8084/rest/clientes';

  constructor(private http: HttpClient) { }

  list() {
    return this.http.get<Cliente[]>(this.API)
      .pipe(
        delay(2000),
        tap(console.log)
      );
  }

  loadByCod(codigo) {
    return this.http.get<Cliente[]>(`${this.API}/${codigo}`);
 }


  //public loadByCod(codigo) {
 // return this.http.get<Cliente>(`${this.API}/${codigo}`).pipe(map((res: Cliente) => res.codigo));
  //}

  create(cliente) {
    return this.http.post(this.API, cliente).pipe(take(1));
  }

  update(cliente) {
    return this.http.put(`${this.API}/${cliente.codigo}`, cliente).pipe(take(1));
  }

  remove(codigo){
    return this.http.delete(`${this.API}/${codigo}`).pipe(take(1));
  }

}
