import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { Component, OnInit, NgModule } from '@angular/core';
import { ClienteService } from '../service/cliente.service';
import { Location } from '@angular/common'
import { Cliente } from '../clientes-lista/cliente';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-clientes-form',
  templateUrl: './clientes-form.component.html',
  styleUrls: ['./clientes-form.component.css']
})
export class ClientesFormComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private location: Location,
    private fb: FormBuilder,
    private service: ClienteService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe((params: any) => {
      const cod = params['id'];
      console.log(cod);
        const cliente$ = this.service.loadByCod(cod);
        cliente$.subscribe((cliente: Cliente[]) => {
          this.updateForm(cliente[0]);
        });
    });

    this.form = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      loja: [null, Validators.required],
      nomeFantasia: [null, Validators.required],
      endereco: [null, Validators.required],
      tipo: [null, Validators.required],
      estado: [null, Validators.required],
      municipio: [null, Validators.required]
    });
  }

  updateForm(cliente: Cliente): void {
    this.form.setValue(cliente)

    // this.form.setValue({
    //   id: cliente.id,
    //   nome: cliente.nome,
    //   loja: cliente.loja,
    //   nomeFantasia: cliente.nomeFantasia,
    //   endereco: cliente.endereco,
    //   tipo: cliente.tipo,
    //   estado: cliente.estado,
    //   municipio: cliente.municipio
    // });
  }


  hasError(field: string): ValidationErrors {
    return this.form.get(field).errors;
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.form.valid);
    console.log(this.form.value.id);
    
    this.route.params.subscribe((params: any) => {
      const cod = params['id'];

      if (this.form.valid && cod) {
        console.log('if');
        this.service.update(this.form.value).subscribe(
          success => {
            console.log('sucesso');
            this.location.back();
          },
          error => console.error(error),
          () => console.log('update ok')
        )
      } else {
        console.log('else');

        this.service.create(this.form.value).subscribe(
          success => {
            console.log('sucesso');
            this.location.back();
          },
          error => console.error(error),
          () => console.log('request ok')
        );
      }
    })
  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
  }
}
