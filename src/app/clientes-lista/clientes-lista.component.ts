import { Cliente } from './cliente';
import { Observable, empty } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ClienteService } from '../service/cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.css'],
  preserveWhitespaces: true
})
export class ClientesListaComponent implements OnInit {

  //clientes: Cliente[];

  deleteModalRef: BsModalRef;
  clientes$: Observable<Cliente[]>;

  clienteSelect: Cliente;

  @ViewChild('deleteModal') deleteModal;

  constructor( 
    private router: Router,
     private service: ClienteService,
      private route: ActivatedRoute,
       private modalService: BsModalService) { }

  ngOnInit() {
    //this.service.list().subscribe(dados => this.clientes = dados);
  this.clientes$ = this.service.list();
  }

  onRefresh() {
    this.clientes$ = this.service.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );
  }
  handleError() {
    throw new Error("Method not implemented.");
  }


  onEdit(codigo:number){
    console.log(codigo);
    this.router.navigate(['clientes/alterar/', codigo]);
  }

  onDelete(cliente){
    this.clienteSelect  = cliente
    this.deleteModalRef = this.modalService.show(this.deleteModal, {class: 'modal-sm'});
  }

  onConfirmDelete(){
    this.service.remove(this.clienteSelect.id)
    .subscribe(
      success => {this.onRefresh(),
      this.deleteModalRef.hide()},
      error => console.error(error)
    );
  }

  onDeclineDelete(){
    this.deleteModalRef.hide();
  }
}
