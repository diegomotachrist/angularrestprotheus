export interface Cliente {
    id: string;
    nome: string;
    loja: string;
    nomeFantasia: string;
    endereco: string;
    tipo: string;
    estado: string;
    municipio: string;
}